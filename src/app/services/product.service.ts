import { Injectable, importProvidersFrom } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { Product } from '../models/product';
import { trigger } from '@angular/animations';
import { catchError, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  errorMsg: string="";
  products : Product[] = [];
url : string="http://localhost:5136/api/product";
  constructor(private _http : HttpClient) { }
  // getProducts() 

  // {
  //   return this._http.get(this.url);
  // }

  getProducts() 
  {
    return this._http.get(this.url)
  }    
}
