import { Component } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {
  /**
   *
   */
  constructor(private _productService : ProductService) {
    
    
  }
products : any[] = [];
ngOnInit()
{
  this._productService.getProducts().subscribe(
    ((res) => console.log(res)
    ),
    (err:any)=>
    {
      // console.log("aaaaaaaaa" + err)
      this.handleError(err);
      })
  }
  handleError(err: any)
  {
     alert(err);
  }
   
}

